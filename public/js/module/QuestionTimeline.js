(function() {

	var Question = Backbone.Model.extend({

	}); // end question model

	var QuestionCollection = Backbone.Collection.extend({
		model: Question
	}); // end question collection

	var QuestionView = Backbone.View.extend({
		
		tagName: 'li',
		className: 'question',
		template: tpl('box-question'),

		events: {
			"click .btn-show-answer": "showFormAnswer",
			"blur textarea": "hideFormAnswerIfEmpty",
			"submit": "submitAnswer",
			"keyup textarea": "handleFormAnswer",
		},

		render: function() {
			var layout = this.template(this.model.toJSON());

			this.$el.html(layout);

			if(this.model.get("allowAnswer")) {
				this.$("textarea.answer").autogrow({speed:0});
			}

			this.$el.attr("id", 'q'+this.model.get("_id"));
			this.$textarea = this.$("textarea");
			this.$boxQuestion = this.$(".box-question");
			this.$boxAnswer = this.$(".box-answer");
			this.$btnShow = this.$(".btn-show-answer");
			this.$btnSubmit = this.$(".btn-answer");

			return this;
		},

		submitAnswer: function(e) {
			e.preventDefault();

			var answer = this.$textarea.val().trim();
			var url = '/answer/'+this.model.get("_id");
			var View = this;

			if(!answer) return false;

			$.post(url, {answer: answer})
			.done(function(updated_question) {
				View.$boxAnswer.remove();
				View.$btnShow.remove();
			})	
			.fail(function() {
				alert("Something went wrong, see terminal for log error");
			});
		},

		answer: function(question_data) {
			this.model.set(question_data);

			var $answer = this.$boxQuestion.find("p");
			var answer = this.model.get("answer");

			this.$boxQuestion.append("<p>"+answer+"</p>");
		},

		hideFormAnswerIfEmpty: function() {
			var answerLength = this.$textarea.val().trim().length;
			var view = this;
			if(answerLength == 0) {
				setTimeout(function() {
					view.$boxAnswer.addClass("hide");
					view.$btnShow.removeClass("hide");
				}, 100);
			}
		},

		showFormAnswer: function() {
			this.$boxAnswer.removeClass("hide");
			this.$btnShow.addClass("hide");
			this.$textarea.focus();
		},

		handleFormAnswer: function() {
			var answerLength = this.$textarea.val().length;

			if(answerLength > 0) {
				this.$btnSubmit.removeAttr("disabled");
			} else {
				this.$btnSubmit.attr("disabled", "disabled");
			}
		}

	}); // end question view

	window.QuestionTimeline = Backbone.View.extend({

		maxId: null,
		timelineUser: false,
		timelineUrl: '/json/timeline',

		events: {
			"click button.btn-loadmore": "loadMore",
		},

		viewQuestions: {},

		initialize: function(options) {
			this.timelineUser = this.$el.data("user");
			this.timelineUrl = this.$el.data("timeline");
			this.enableAnswer = options.enableAnswer? true : false;
			this.showUserFrom = options.showUserFrom? true : false;
			this.showUserTo = options.showUserTo? true : false;

			this.viewQuestions = {};

			this.$listQuestion = this.$(".list-questions");
			this.$loadmore = this.$("button.btn-loadmore");
			
			this.collection = new QuestionCollection();
			this.listenTo(this.collection, 'add', this.renderQuestionView);
		},

		loadMore: function() {
			var This = this;
			var data = {};

			if(this.maxId) data.maxId = this.maxId;

			$.getJSON(this.timelineUrl, data, function(data) {
				$.each(data.questions, function(i, question_data) {
					This.pushQuestion(question_data, true);
				});

				if(data.questions.length) {
					This.maxId = data.questions.pop()._id;
				} else {
					This.disableLoadMoreButton();
				}
			});
		},

		disableLoadMoreButton: function() {
			this.$loadmore.attr("disabled", "disabled").text("udah mentok bro");
		},

		pushQuestion: function(question_data, append) {
			question_data.append = append? 'append' : 'prepend';
			question_data.allowAnswer = this.enableAnswer;
			question_data.showUserFrom = this.showUserFrom;
			question_data.showUserTo = this.showUserTo;

			var question = new Question(question_data);
			this.collection.add(question);
		},

		answerQuestion: function(question_data) {
			var viewQuestion = this.viewQuestions[question_data._id];
			viewQuestion.answer(question_data);
		},

		renderQuestionView: function(question) {
			var questionView = new QuestionView({model: question});
			var append_or_prepend = question.get('append');

			var elQuestionView = questionView.render().el;

			this.viewQuestions[question.get('_id')] = questionView;
			console.log(this.viewQuestions);

			switch(append_or_prepend) {
				case 'append': this.$listQuestion.append(elQuestionView);
					break;
				case 'prepend': this.$listQuestion.prepend(elQuestionView);
					break;
			}
		},

	}); // end timeline view

})()
