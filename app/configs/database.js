module.exports = {

	default_connection: "single",

	connections : {

    "single": {
      
      enable: true,

      username: "",
      password: "",
      host: "127.0.0.1",
      port: 27017,
      database: "testapp1",

    },
    
    "replication_example": {

      enable: false,

      replica: [
        {
          username: "",
          password: "",
          host: "127.0.0.1",
          port: 27001,
          database: "testapp1",
        },
        {
          username: "",
          password: "",
          host: "127.0.0.1",
          port: 27002,
          database: "testapp1",
        },
        {
          username: "",
          password: "",
          host: "127.0.0.1",
          port: 27003,
          database: "testapp1",
        }
      ],

      options: {
        replset: {
          strategy: "ping",
          rs_name: "myrepl"
        }
      }

    }, // end replication_example connection
		
	},

};