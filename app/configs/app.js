module.exports = {

	name: 'Alague',

	port: 3000,

  // enable sockets
	socket: true,

  // enable session
	session: true,


  /* -----------------------------------------
   * EXPRESS 4 SETTINGS 
   * -----------------------------------------
   */

  // Trust Proxy
  // if it set to false, 'trust proxy' will disabled
  // set it with 'trust proxy' value if you want fo enable it
  trust_proxy: false,

  // Case Sensitive Route
  // if it set to true, 'GET /Foo' != 'GET /foo'
  route_case_sensitive: false,

  // Strict Route
  // if it set to true, user can't access 'GET /foo' with 'GET /foo/'
  route_strict: false,

  // Json Replacer
  // set to function that modify json value before we send it to client
  // set it to non-function will disable it
  json_replacer: false,

  // Json Beautify
  // if it set to true, when you send response with res.json(data),
  // it will pretiffy that json to client
  json_beautify: false,

  // Jsonp Callback
  // jsonp callback name when you send response with res.jsonp(data)
  jsonp_callback: 'callback',

  // Subdomain Offset
  // The number of dot-separated parts of the host to remove to access subdomain
  // default is 2
  subdomain_offset: 2,

  // ETag
  // ETag response header
  // available options: 'weak', 'strong' or function
  etag: true,

  // Query Parser
  // The query parser to use - "simple" or "extended"
  // defaults to "extended"
  query_parser: 'extended',

  // X-Powered-By
  // Enables the "X-Powered-By" HTTP Headers, enabled by default
  poweredBy: 'Alague'

};