module.exports = {

	pageLogin: function(req, res, next) {
		res.render("theme/pages/login");
	},

	login: function(req, res, next) {
		var username = req.body.username;
		var password = req.body.password;

		User.findByUsername(username, function(err, user) {
			if(err) return next(err);

			if(!user) {
				res.flash("error", "User tidak ditemukan");
				return res.redirect("/login");
			}

			if(!user.passwordMatch(password)) {
				res.flash("error", "Password salah");
				return res.redirect("/login");
			}

			req.session.user = user.toJSON();

			delete req.session.user.password; // remove password from session
			
			res.flash("info", "Login berhasil");
			res.redirect("/");
		});
	},

	logout: function(req, res, next) {
		if(req.session.user) {
			delete req.session.user;
		}

		res.flash("info", "Anda telah logout");
		res.redirect("/");
	}

}