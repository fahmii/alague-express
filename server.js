
var Alague = require("./alague");

var App = new Alague({

	// public directory
	public: __dirname+"/public",

	// configs path
	configs: __dirname+"/app/configs",

	// controllers path
	controllers: __dirname+"/app/controllers",

	// views path
	views: __dirname+"/app/views",

	// models path
	models: __dirname+"/app/models"

});

App.run();