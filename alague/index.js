var _ = require("lodash"),
		fs = require("fs"),
		http = require('http'),
		debug =	require("debug")('alague:core'),
		Express = require('express'),
		bodyParser = require("body-parser"),
		Promise = require("bluebird");		

var	loader = require("./loader.js"),
		flash = require("./flash.js"),
		Config = require("./configurator.js"),
		EventDispatcher = require("./event-dispatcher.js");

var default_config_starter = {
	public: __dirname+"/../public",
	views: __dirname+"/../app/views",
	controllers: __dirname+"/../app/controllers",
	models: __dirname+"/../app/models",
	configs: __dirname+"/../app/configs",
	helpers: __dirname+"/../app/helpers",
	environments: {}
};

function initializeConfigs(app) {
	var config = new Config(app);
	config.loadExcepts(["routes", "events", "socket"]);

	return config;
}

function initializeEvent(app) {
	var events = app.config.load("events");
	var app_events = require("./events.js");
	var eventDispatcher = new EventDispatcher(app);
	
	for(evt in app_events) {
		eventDispatcher.on(evt, app_events[evt]);
	}

	for(evt in events) {
		eventDispatcher.on(evt, events[evt]);
	}

	return eventDispatcher;
}

function initializeExpress(app) {
	var express = Express();

	var config_alias = {
		'trust_proxy': 'trust proxy',
		'route_case_sensitive': 'case sensitive routing',
		'route_strict': 'strict routing',
		'json_replacer': 'json replacer',
		'json_beautify': 'json spaces',
		'jsonp_callback': 'jsonp callback',
		'subdomain_offset': 'subdomain offset',
		'etag': 'etag',
		'query_parser': 'query parser',
		'poweredBy': 'x-powered-by'
	};

	for(key in config_alias) {
		var exKey = config_alias[key];
		express.set(exKey, app.config.get("app."+key));
	}

	return express;
}

function setupSession(app) {
	var sessionConfig = app.config.get('session');

	var session = require('express-session');
	switch(sessionConfig.store) {
		case 'redis':
			var RedisStore = require('connect-redis')(session);
			sessionConfig.store = new RedisStore(sessionConfig.redis);

			debug("Enabling session RedisStore");
			break;

		case 'memory':
			sessionConfig.store = new session.MemoryStore();

			debug("Enabling session MemoryStore");
			break;
	}

	if(app.config.get("app.socket") && !sessionConfig.store) {
		debug("Socket and Session has been enabled, but no session store. So we enabling session MemoryStore...");
		sessionConfig.store = new session.MemoryStore();
	}
		
	app.sessionStore = sessionConfig.store;

	app.express.use(session(sessionConfig));
	app.express.use(flash);

	debug("Setup session done");
}

function setupViewEngine(app) {
	app.express.engine('jade', require('jade').__express);
	app.express.set('views', app.__configs.starter.views);
	app.express.set('view engine', 'jade');

	app.express.use(function(req, res, next) {
		app.express.locals.App = app;
		app.express.locals.req = req;
		app.express.locals.res = res;
		next();
	});
}


var Alague = function(config_starter) {
	if(!global.App) {
		global.App = this;
		global.Controller = {};
	}

	var self = this;
	config_starter = _.extend(default_config_starter, config_starter);
	
	this.__configs = {
		starter: config_starter,
		environment: {},
		general: {}
	};

	this.environment = process.env.NODE_ENV = process.env.NODE_ENV || 'development';
	this.configs = {};
	this.routes = {};
	this.models = {};
	this.runningAt = null;
	this.config = initializeConfigs(this);
	this.express = initializeExpress(this);
	this.server = http.Server(this.express);
	this.event = initializeEvent(this);
};

Alague.prototype.prepare = function(callback) {
	var self = this;

	this.ready = false; 
	this.event.fire("boot.helper.before", this);
	this.helpers = loader.loadHelpers(this);
	this.event.fire("boot.helper.after", this);

	this.event.fire("boot.controller.before", this);
	this.controllers = loader.loadControllers(this);
	this.event.fire("boot.controller.after", this);

	if(true === this.config.get("app.session")) {
		debug("Setup session");
		this.event.fire("boot.session.before", this);
		setupSession(this);
		this.event.fire("boot.session.after", this);
	} else {
		debug("Disable session");
	}

	if(true === this.config.get("app.socket")) {
		debug("Setup socket");
		this.event.fire("boot.socket.before", this);
		this.io = loader.loadSocket(this);
		this.event.fire("boot.socket.after", this);
	} else {
		debug("Disable socket");
	}

	this.event.fire("boot.view.before", this);
	setupViewEngine(this);
	this.event.fire("boot.view.after", this);

	this.event.fire("boot.routes.before", this);
	this.route = loader.loadRoutes(this);
	this.event.fire("boot.routes.after", this);

	this.event.fire("boot.connection.before", self);
	loader.loadDatabaseConnections(this, function(connectionManager) {
		self.connection = connectionManager;
		self.event.fire("boot.connection.after", self);

		self.event.fire("boot.model.before", self);
		self.models = loader.loadModels(self);
		self.event.fire("boot.model.after", self);

		self.ready = true;
		self.event.fire("app.ready");
		callback(self);
	});

};

Alague.prototype.run = function(callback) {
	var self = this;

	if(true !== this.ready) {
		this.prepare(function() {
			self.run(callback);
		});
	} else {
		//delete self.ready;
		var port = self.config.get("app.port");
		var environment = self.environment;

		self.server.listen(port, function() {
			self.event.fire("app.run");

			console.log("App run on port %d in environment %s", port, environment);
			if(typeof callback == "function") callback();
		});
	}
};

Alague.prototype.stop = function() {
	process.stop();
};

module.exports = Alague;