var debug = require("debug")("alague:route");

function parsePath(pathString) {
  debug("Parsing route "+pathString);

  var allowed_methods = ['GET', 'POST', 'PUT', 'PATCH', 'DELETE', 'ANY'];
  var splitPath = pathString.split(" ");
  
  var regexName = /as \w+$/;
  var hasName = regexName.test(pathString);
  var method_allowed = false;

  var method = splitPath[0];
  var path = splitPath[1];
  var name;

  if(!method || !path) {
    throw new Error("Route must formatted like '{http_method} {path}'");
  }

  method = method.toUpperCase();

  for(i in allowed_methods) {
    if(method == allowed_methods[i]) {
      method_allowed = true;
      break;
    }
  }

  if(!method_allowed) {
    throw new Error("Route method is not available");
  }

  if(hasName) {
    var name = pathString.match(regexName).join('').split(' ')[1];
  }

  return {
    name: name, 
    method: method,
    path: path
  };
}

var id = 0;

var Route = function(path, middlewares) {
  this.id = ++id;
  
  if(typeof middlewares == "function") {
    middlewares = [middlewares];
  }

  var routeStr = path;
  var parsedPath = parsePath(path);
  var path = parsedPath.path;
  var name = parsedPath.name;
  var method = parsedPath.method;

  this.getDefinedRoute = function() {
    return routeStr;
  }

  this.getMiddlewares = function() {
    return middlewares;
  }

  this.getName = function() {
    return name;
  }

  this.getPath = function() {
    return path;
  }

  this.getMethod = function() {
    return method;
  }

};

var Router = function(app_or_routename) {

  if(!this instanceof Router) {
    return App.route.getPathByName(app_or_routename);
  }

  this.app = app_or_routename;
  this.app.route = this;
  this.__named_routes = {};
  this.__routes = {};
}

Router.prototype.getPathByName = function(name) {
  var route = this.__named_routes[name];

  if(!route) {
    throw new Error("There is no route named "+name);
  } else {
    return route.getPath();
  }
};

Router.prototype.getRoutes = function() {
  return this.__routes;
};

Router.prototype.register = function(route) {
  if(!route instanceof Route) {
    throw new Error("Route must be instanceof Router.Route");
  }

  var self = this;
  this.__routes[route.id] = route;

  if(route.getName()) {
    this.__named_routes[route.getName()] = route;
  }

  var middlewares = route.getMiddlewares();

  route.getMiddlewares().forEach(function(middleware) {
    self.app.express[route.getMethod().toLowerCase()](route.getPath(), middleware);
  });

  debug("Register route [" + route.id + "]: " + route.getDefinedRoute() + " with " + route.getMiddlewares().length + " middleware(s)");
};

Router.Route = Route;

module.exports = Router;