var Alague = require("./index"),
    debug = require("debug")("alague:event");

var EventDispatcher = function(app) {
  var events = {};

  function isMatchEvent(called_event, event_name) {
    if(called_event == event_name) return true;

    // if event_name end with *, 
    if(event_name.match(/\.\*$/)) {
      // remove that, for regex
      event_regex_str = event_name.replace(/\.\*$/, '');
      event_regex_str = event_name.replace('.', '\\.');
      
      var regex = new RegExp("^"+event_regex_str);

      return called_event.match(regex)? true : false;
    } else {
      return false;
    }
  }

  this.on = function(evt, callback) {
    if(typeof callback != "function") {
      throw new Error("Event callback must be function");
    }

    if(!events[evt]) {
      events[evt] = [];
    }

    debug("Register event " + evt);
    events[evt].push(callback);
  };

  this.fire = function(evt) {
    debug("Firing event "+evt);

    for(evt_key in events) {
      if(isMatchEvent(evt, evt_key)) {
        debug("Fire " + events[evt_key].length + " " + evt_key + " events");
        for(i in events[evt_key]) {
          events[evt_key][i].apply(null, arguments);
        }
      }
    }
  };

}

module.exports = EventDispatcher;