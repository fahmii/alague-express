var _ = require("lodash"),
		debug = require("debug")("alague:db-connector"),
		mongoose = require("mongoose"),
		Promise = require("bluebird");


function buildConnectionString(conn) {
	// if use replica
	if(conn.replica) {
		var connStrings = [];
		for(i in conn.replica) {
			connStrings.push(buildConnectionString(conn.replica[i]));
		}

		return connStrings.join(", ");
	}

	conn = _.extend({
		username: "",
		password: "",
		host: "127.0.0.1",
		port: false,
		database: "test",
	}, conn);

	var auth = conn.username? conn.username+":"+conn.password+"@" : "";
	var hostport = conn.port? conn.host+":"+conn.port : conn.host;

	return "mongodb://"+auth+hostport+"/"+conn.database;
}

var Connection = function(appOrConnectionName) {
	
	this.app = appOrConnectionName;
	this.app.mongoose = mongoose;
	this.__connections = {};
	this.__default_connection = "default";

}

Connection.prototype.getConnection = function(name) {
	return this.__connections[name];
};

Connection.prototype.setDefaultConnection = function(name) {
	this.__default_connection = name;

	return this;
}

Connection.prototype.addConnection = function(name, connection) {
	var self = this;
	var connectionString = buildConnectionString(connection);
	var connectionOptions = connection.options;

	debug("Trying to connect "+connectionString);

	return new Promise(function(resolve, reject) {
		var conn = mongoose.createConnection(connectionString, connectionOptions);
		
		conn.on('open', function() {
			debug("Connected to "+connectionString);
			self.__connections[name] = conn;
			resolve(conn);
		});

		conn.on('error', function(err) {
			err.message = "Failed to connect '"+connectionString+"': "+err.message;
			err.connectionString = connectionString;
			err.connectionOptions = connectionOptions;

			reject(err);
		});
	});
}

Connection.prototype.createConnections = function(connections, callback) {
	var promises = [];

	for(name in connections) {
		var connection = connections[name];
		promises.push(this.addConnection(name, connection));
	}

	return Promise.all(promises);
}


Connection.prototype.createAppConnections = function(callback) {
	var promises = [];
	var connections = this.app.config.get("database.connections");

	for(name in connections) {
		if(false === connections[name].enable) continue;

		var connection = connections[name];
		promises.push(this.addConnection(name, connection));
	}

	return Promise.all(promises);
}

module.exports = function(app) {
	return new Connection(app);
}