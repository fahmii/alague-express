var _ = require("lodash"),
    fs = require("fs"),
    util = require("util"),
    Alague = require("./index"),
    debug = require("debug")("alague:config");

var Config = function(app) {
  var configs_dir = app.__configs.starter.configs;
  var configs_env_dir = configs_dir+"/"+app.environment;

  this.app = app;
  this.__dir = app.__configs.starter.configs;
  this.__env_dir = fs.existsSync(configs_env_dir)? configs_env_dir : null;
};

function getValue(obj, keyPath, default_value) {
  var splitKeys = keyPath.split(".");
  var firstKey = splitKeys.shift();
  var value = default_value;

  if(undefined === obj[firstKey]) return default_value;

  value = obj[firstKey];

  while(splitKeys.length) {
    var k = splitKeys.shift();
    var isLast = (splitKeys.length == 0);

    if(typeof value !== "object") {
        value = default_value;
        break;
    } else {
      if(null === value && !isLast) {
        value = {};
      }
      
      if(undefined === value[k]) {
        value = default_value;
        break;
      } else {
        value = value[k];
      }
    }
  }

  return value;
};

function setValue(obj, keyPath, value) {
  var splitKeys = keyPath.split(".");
  var keyNow = splitKeys.shift();
  var nextPath = splitKeys.join(".");

  switch(typeof obj[keyNow]) {
    case "string":
      obj[keyNow] = new String(obj[keyNow]);
      break;

    case "number":
      obj[keyNow] = new Number(obj[keyNow]);
      break;

    case "object":
      obj[keyNow] = (null === obj[keyNow])? {} : obj[keyNow];
      break;

    default: obj[keyNow] = {};
  }

  if(!nextPath) {
    obj[keyNow] = value;
    return 1;
  } else {
    return setValue(obj[keyNow], nextPath, value);
  }
}


Config.prototype.load = function(file) {
  var config_file = this.__dir+"/"+file+".js";
  var config_env_file = this.__env_dir? this.__env_dir+"/"+file+".js" : null;

  if(fs.existsSync(config_file)) {
    this.app.__configs.general[file] = require(config_file);
    this.app.configs[file] = this.app.__configs.general[file];
    debug("Load general config: "+file);
  }

  if(config_env_file && fs.existsSync(config_env_file)) {
    this.app.__configs.environment[file] = require(config_file);
    this.app.configs[file] = _.extend(
      _.clone(this.app.__configs.general[file] || {}, true), 
      _.clone(this.app.__configs.environment[file], true)
    );

    debug("Load environment config: "+file);
  }

  return this.app.configs[file];
};

Config.prototype.loadExcepts = function(excepts) {
  if(!util.isArray(excepts)) {
    debug("Call config.loadExcepts but excepts is " + (typeof excepts) + " instead array. So we load all configs");
    excepts = [];
  }

  var config_files = fs.readdirSync(this.__dir);
  var config_env_files = this.__env_dir? fs.readdirSync(this.__env_dir) : [];
  var returnConfigs = {};


  for(i in config_files) {
    var file = config_files[i];
    var cfg_name = file.replace(/\.js$/, '');
    var config_file = this.__dir+"/"+file;

    if(excepts.indexOf(cfg_name) > -1 || false === /\.js$/.test(file)) {
      continue;
    }
    
    this.app.__configs.general[cfg_name] = require(config_file);
    this.app.configs[cfg_name] = this.app.__configs.general[cfg_name];

    debug("Load general config: "+cfg_name);

    returnConfigs[cfg_name] = _.clone(this.app.__configs.general[cfg_name], true);
  }

  for(i in config_env_files) {
    var file = config_files[i];
    var cfg_name = file.replace(/\.js$/, '');
    var config_file = this.__dir+"/"+file;

    if(excepts.indexOf(cfg_name) > -1 || false === /\.js$/.test(file)) {
      continue;
    }

    this.app.__configs.environment[cfg_name] = require(config_file);
    this.app.configs[cfg_name] = _.extend(
      _.clone(this.app.__configs.general[cfg_name] || {}, true), 
      _.clone(this.app.__configs.environment[cfg_name], true)
    );

    debug("Load environment config: "+cfg_name);

    returnConfigs[cfg_name] = this.app.configs[cfg_name];
  }

  return returnConfigs;
};

Config.prototype.loadAll = function() {
  return this.loadExcepts([]);
};

Config.prototype.get = function(key, default_value) {
  return getValue(this.app.configs, key, default_value);
};

Config.prototype.set = function(key, value) {
  setValue(this.app.configs, key, value);
};

module.exports = Config;