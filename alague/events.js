var Express = require("express"),
    bodyParser = require("body-parser");

function errorHttpHandler(req, res, next) {
  res.notFound = function(data) {
    return App.event.fire("route.error.404", req, res, next, data);
  };

  res.abort = function(data) {
    return App.event.fire("route.error.500", req, res, next, data);
  };

  next();
}

module.exports = {

  "boot.routes.before": function(evt, app) {
    app.express.use(Express.static(app.__configs.starter.public));
    app.express.use(bodyParser.json()); // for parsing application/json
    app.express.use(bodyParser.urlencoded({ extended: true })); // for parsing application/x-www-form-urlencoded
    app.express.use(errorHttpHandler);
  },

  "boot.routes.after": function(evt, app) {
    app.express.use(function(req, res, next) {
      res.status(404);
      console.log("Call to undefined route "+req.url);
      app.event.fire("route.error.404", req, res, next);
    });

    app.express.use(function(error, req, res, next) {
      res.status(500);
      app.event.fire("route.error.500", error, req, res, next);
    });
  },

}